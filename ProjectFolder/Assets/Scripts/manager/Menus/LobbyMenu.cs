﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LobbyMenu : MyNetworkManager {
    private Vector2 scrollPosition = new Vector2(0.0f, 0.0f);

    private string numOfPlayersText = "";
    private string gameName = "";
    private int numOfPlayers;
    private string gameDescription = "";
    private string passWord = "";
    private List<string> currentPassword = new List<string>();

    private List<string> errorsListCreation = new List<string>();
    private List<string> errorListJoin = new List<string>();
    public static HostData currentGame = null;
    private float waitForUpdateList = 3.0f;
    private float currentTime = 0.0f;
    private int state = 0;
    private int step = 0;

    private const float sendHostTimer = 1.0f;
    private float sendHostCountdown = 0.0f;
    public void Start()
    {
        StartCoroutine(RefreshHostList());
        step = 0;
    }

    public void Update()
    {
        currentTime += Time.deltaTime;
        if(currentTime >= waitForUpdateList)
        {
            currentTime = 0.0f;
            StartCoroutine(RefreshHostList());
            bool found = false;
            foreach(HostData hd in getHostData())
            {
                if (currentGame != null)
                {
                    step++;
                    if (hd.gameName.Equals(currentGame.gameName))
                    {
                        currentGame = hd;
                        found = true;
                    }
                    if (step >= 5)
                    {
                        step = 0;
                        bool trueConnection = false;
                        for (int index = 0; index < sharedPlayers.Count; index++)
                        {
                            if ((PlayerPrefs.GetString(CreateProfile.us).Equals(sharedPlayers[index]))
                                || (PlayerPrefs.GetString(CreateProfile.us) + ": Host").Equals(sharedPlayers[index]))
                            {
                                trueConnection = true;
                            }
                        }
                        if (!trueConnection)
                            currentGame = null;
                    }
                }
            }
            if(!found)
            {
                currentGame = null;
            }
        }
        if (state == 1)
        {
            if(Network.isServer)
            {
                sendHostCountdown += Time.deltaTime;
                if(sendHostCountdown >= sendHostTimer)
                {
                    networkView.RPC("sendState", RPCMode.AllBuffered, 2);
                    Application.LoadLevel("GameScene");
                }
            }
            else
            {
                Application.LoadLevel("GameScene");
            }
        }
        else if(state == 2)
        {
            if(!Network.isServer)
            {
                Network.Disconnect(200);
                errorListJoin.Add("The game is already in progress");
            }
        }
    }

    private IEnumerator WaitForStuff()
    {
        yield return new WaitForSeconds(3f);
    }

    private IEnumerator WaitForConnect(int i)
    {
        yield return new WaitForSeconds(4f);
        Network.Connect(getHostData()[i], currentPassword[i]);
    }

	public void OnGUI()
    {
        if (!getInit())
            Initialize();

        GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "Game Lobby Menu Screen", GetBoxStyle());

        //outline around scene
        Drawing.DrawLine(new Vector2(0, 2), new Vector2(Screen.width, 2), Color.black, GetLineWidth()); //TL -> TR
        Drawing.DrawLine(new Vector2(Screen.width - 1, 1), new Vector2(Screen.width - 1, Screen.height - 1), Color.black, GetLineWidth()); //TR -> BR
        Drawing.DrawLine(new Vector2(2, 0), new Vector2(2, Screen.height), Color.black, GetLineWidth());// TL -> BL
        Drawing.DrawLine(new Vector2(2, Screen.height - 3), new Vector2(Screen.width, Screen.height - 3), Color.black, GetLineWidth()); // BL -> BR

        //outline to seperate sections of menu
        Drawing.DrawLine(new Vector2(0, Screen.height * 0.05f), new Vector2(Screen.width, Screen.height * 0.05f), Color.black, GetLineWidth()); // Menu title _ Everything else
        Drawing.DrawLine(new Vector2(Screen.width * 0.3f, Screen.height * 0.05f), new Vector2(Screen.width * 0.3f, Screen.height), Color.black, GetLineWidth()); // Create | Join
        Drawing.DrawLine(new Vector2(Screen.width * 0.7f, Screen.height * 0.05f), new Vector2(Screen.width * 0.7f, Screen.height), Color.black, GetLineWidth()); // Join | Details
        Drawing.DrawLine(new Vector2(Screen.width * 0.3f, Screen.height * 0.9f), new Vector2(Screen.width * 0.7f, Screen.height * 0.9f), Color.black, GetLineWidth()); // bottom section

        //Creates the create lobby Section
        CreateLobbySection();

        //Creates the join lobby section
        JoinLobbySection();

        //Creates Gui for current lobby details
        LobbyDetailsSection();

        OtherSection();
    }

    private void CreateLobbySection()
    {
        MakeTitle(new Vector2(Screen.width * 0.09f, Screen.height * 0.1f), "Create New Lobby");

        GUI.Label(new Rect(0f, Screen.height * 0.18f, Screen.width * 0.3f, Screen.height * 0.1f), "Game Name", GetLabelStyle());
        gameName = GUI.TextField(new Rect(Screen.width * 0.089f, Screen.height * 0.25f, Screen.width * 0.12f, 30f), gameName, 25, GetTextFieldStyle());

        GUI.Label(new Rect(0f, Screen.height * 0.3f, Screen.width * 0.3f, Screen.height * 0.1f), "Number of Players (1-4)", GetLabelStyle());
        numOfPlayersText = GUI.TextField(new Rect(Screen.width * 0.1375f, Screen.height * 0.375f, Screen.width * 0.02f, 30f), numOfPlayersText, 1, GetTextFieldStyle());

        GUI.Label(new Rect(0f, Screen.height * 0.425f, Screen.width * 0.3f, Screen.height * 0.1f), "Game Description", GetLabelStyle());
        gameDescription = GUI.TextField(new Rect(Screen.width * 0.089f, Screen.height * 0.5f, Screen.width * 0.12f, 30f), gameDescription, 50, GetTextFieldStyle());

        GUI.Label(new Rect(0f, Screen.height * 0.56f, Screen.width * 0.3f, Screen.height * 0.1f), "Password(optional)", GetLabelStyle());
        passWord = GUI.TextField(new Rect(Screen.width * 0.1f, Screen.height * 0.625f, Screen.width * 0.1f, 30f), passWord, 10, GetTextFieldStyle());
    
        for(int i = 0; i < errorsListCreation.Count; i++)
        {
            GUI.Label(new Rect(Screen.width * 0f, Screen.height * (0.675f + i * 0.05f), Screen.width * 0.3f, Screen.height * 0.05f), errorsListCreation[i], GetErrorStyle());
        }

        if(GUI.Button(new Rect(Screen.width * 0.095f, Screen.height * 0.85f, Screen.width * 0.1f, Screen.height * 0.05f), "Create Lobby", GetButtonStyle()))
        {
            errorsListCreation.Clear();
            CheckForErrors();
            if(errorsListCreation.Count == 0)
            {
                StartCoroutine(CreateLobby());
            }
        }
    }

    private IEnumerator CreateLobby()
    {
        StartServer(gameName, gameDescription, numOfPlayers, passWord);
        isRefreshing = true;
        StartCoroutine(RefreshHostList());
        yield return new WaitForSeconds(1f);
        if(getHostData() != null)
        {
            foreach(HostData hd in getHostData())
            {
                if (hd.gameName.Equals(gameName))
                {
                    currentGame = hd;
                }
            }
        }
    }

    private void JoinLobbySection()
    {
        MakeTitle(new Vector2(Screen.width * 0.4375f, Screen.height * 0.1f), "Join Existing Lobby");
        if (getHostData() != null)
        {
            for (int i = 0; i < getHostData().Length; i++)
            {
                currentPassword.Add("");
                if (currentGame == null)
                {
                    if (getHostData()[i].passwordProtected && getHostData()[i].connectedPlayers < getHostData()[i].playerLimit - 1)
                    {
                        currentPassword[i] = GUI.TextField(new Rect(Screen.width * 0.6f, Screen.height * (0.16f + i * 0.05f), Screen.width * 0.08f, Screen.height * 0.025f), currentPassword[i], 10, GetTextFieldStyle());
                    }
                }
                if (currentPassword.Count > getHostData().Length)
                    currentPassword.RemoveAt(currentPassword.Count-1);
            }
        }

        GUI.BeginGroup(new Rect(Screen.width * 0.325f, Screen.height * 0.15f, Screen.width * 0.25f, Screen.height * 0.4f));

        scrollPosition = GUILayout.BeginScrollView(scrollPosition, GUILayout.Width(Screen.width * 0.25f), GUILayout.Height(Screen.height * 0.25f));

        if (getHostData() != null)
        {
            for (int i = 0; i < getHostData().Length; i++)
            {
                if (currentGame == null)
                {
                    JoinLobbyChecks(i);
                }
                else if(currentGame.gameName != getHostData()[i].gameName)
                {
                    JoinLobbyChecks(i);
                }
            }
        }
        GUILayout.EndScrollView();

        GUI.EndGroup();

        for (int i = 0; i < errorListJoin.Count; i++)
        {
            GUI.Label(new Rect(Screen.width * 0.4f, Screen.height * (0.7f + i * 0.05f), Screen.width * 0.2f, Screen.height * 0.025f), errorListJoin[i], GetErrorStyle());
        }
    }

    private void JoinLobbyChecks(int i)
    {
        int playerLimit = getHostData()[i].playerLimit;
        if (isSinglePlayer)
            playerLimit = 1;
        if (GUILayout.Button(getHostData()[i].gameName + "\t" + getHostData()[i].connectedPlayers + "/" + playerLimit, GetButtonStyle()))
        {
            errorListJoin.Clear();
            if(currentGame != null)
            {
                bool found = false;
                for (int index = 0; index < sharedPlayers.Count; index++)
                {
                    if(sharedPlayers[i].Equals(PlayerPrefs.GetString(CreateProfile.us)))
                    {
                        found = true;
                    }
                }
                if(found)
                {
                    if (Network.isServer)
                    {
                        Network.Disconnect(200);
                        MasterServer.UnregisterHost();
                    }
                    if (Network.isClient)
                    {
                        Network.Disconnect(200);
                    }
                    currentGame = null;
                }
            }
            if (getHostData()[i].connectedPlayers >= getHostData()[i].playerLimit)
            {
                errorListJoin.Add("Cannot join because there is no room available");
            }
            else
            {
                StartCoroutine(WaitForConnect(i));
                float t = 0.0f;
                do
                {
                    t += 0.1f;
                } while (t < 3.0f);
                if (invalidPassword)
                {
                    errorListJoin.Add("Invalid password");
                    invalidPassword = false;
                }
                else
                {
                    currentGame = getHostData()[i];
                }
            }
        }
    }

    private void LobbyDetailsSection()
    {
        MakeTitle(new Vector2(Screen.width * 0.775f, Screen.height * 0.1f), "Current Lobby Details");

        if(currentGame == null)
        {
            GUI.Label(new Rect(Screen.width * 0.7f, Screen.height * 0.4f, Screen.width * 0.3f, Screen.height * 0.1f), "Currently not part of a lobby", GetLabelStyle());
        }
        else
        {
            int playerLimit = currentGame.playerLimit;
            if (isSinglePlayer)
                playerLimit = 1;
            GUI.Label(new Rect(Screen.width * 0.7f, Screen.height * 0.25f, Screen.width * 0.3f, Screen.height * 0.1f), "Game Name\n" + currentGame.gameName, GetLabelStyle());
            GUI.Label(new Rect(Screen.width * 0.7f, Screen.height * 0.4f, Screen.width * 0.3f, Screen.height * 0.1f), "Game Description\n" + currentGame.comment, GetLabelStyle());
            GUI.Label(new Rect(Screen.width * 0.7f, Screen.height * 0.55f, Screen.width * 0.3f, Screen.height * 0.1f), "Number of Connected Players\n" + currentGame.connectedPlayers + "/" + playerLimit, GetLabelStyle());
            GUI.Label(new Rect(Screen.width * 0.7f, Screen.height * 0.7f, Screen.width * 0.3f, Screen.height * 0.1f), "Player Names", GetLabelStyle());

            for (int i = 0; i < sharedPlayers.Count; i++)
            {
                GUI.Label(new Rect(Screen.width * 0.7f, Screen.height * (0.75f + 0.025f * i), Screen.width * 0.3f, Screen.height * 0.05f), sharedPlayers[i], GetLabelStyle());
            }
            if (Network.isServer)
            {
                if (GUI.Button(new Rect(Screen.width * 0.8f, Screen.height * 0.9f, Screen.width * 0.1f, Screen.height * 0.05f), "Start game", GetButtonStyle()))
                {
                    networkView.RPC("sendState", RPCMode.AllBuffered, 1);
                }
            }

            if(GUI.Button(new Rect(Screen.width * 0.8f, Screen.height * 0.95f, Screen.width * 0.1f, Screen.height * 0.05f), "Disconnect", GetButtonStyle()))
            {
                Network.Disconnect(200);
                if (Network.isServer)
                {
                    MasterServer.UnregisterHost();
                }
                resetPlayerInfo();
                currentGame = null;
            }
        }
    }

    private void OtherSection()
    {
        if(GUI.Button(new Rect(Screen.width * 0.35f, Screen.height * 0.91f, Screen.width * 0.12f, Screen.height * 0.05f), "Return to Main Menu", GetButtonStyle()))
        {
            Application.LoadLevel("main_menu");
        }

        GUIStyle colorBox = CreateCustomBoxColor(PlayerPrefs.GetString(CreateProfile.colour), new Vector3(0.0f, 0.0f, 0.0f), 20);
        GUI.Box(new Rect(Screen.width * 0.475f, Screen.height * 0.91f, Screen.width * 0.025f, Screen.height * 0.05f), "", colorBox);

        GUI.Label(new Rect(Screen.width * 0.5f, Screen.height * 0.835f, Screen.width * 0.2f, Screen.height * 0.2f), "Player name:\n" + PlayerPrefs.GetString(CreateProfile.us), GetLabelStyle());     
    }

    private void MakeTitle(Vector2 pos, string text)
    {
        GUI.Label(new Rect(pos.x, pos.y, Screen.width * 0.3f, 50f), text, GetTitleStyle());
    }

    private void CheckForErrors()
    {
        //checks game name
        if(gameName.Length == 0)
        {
            errorsListCreation.Add("You need to have a Game Name for the Lobby");
        }

        //checks if the number of players is acceptable
        bool success = int.TryParse(numOfPlayersText, out numOfPlayers);
        if (!success)
        {
            errorsListCreation.Add("Enter a numeric value 1-4 inclusive.");
        }
        else
        {
            if (numOfPlayers > 4 || numOfPlayers < 1)
            {
                errorsListCreation.Add("Input a value inside the range of 1-4 inclusive");
            }
        }

        //checks game description
        if(gameDescription.Length == 0)
        {
            errorsListCreation.Add("You need to have a Game Description for the Lobby");
        }
    }

    [RPC]
    public void sendState(int val)
    {
        state = val;
    }
}

