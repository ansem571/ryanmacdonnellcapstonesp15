﻿using UnityEngine;
using System.Collections;

public class ChangeColor : MonoBehaviour 
{
    public void OnTriggerEnter(Collider collider)
    {
        networkView.RPC("ChangeColorToBlue", RPCMode.AllBuffered);
    }

    public void OnTriggerExit(Collider collider)
    {
        networkView.RPC("ChangeColorToWhite", RPCMode.AllBuffered);
    }

    [RPC]
    private void ChangeColorToBlue()
    {
        renderer.material.SetColor("_Color", Color.blue);
    }

    [RPC]
    private void ChangeColorToWhite()
    {
        renderer.material.SetColor("_Color", Color.white);
    }
}
