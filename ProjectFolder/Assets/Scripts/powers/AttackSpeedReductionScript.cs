﻿using UnityEngine;
using System.Collections;

public class AttackSpeedReductionScript : checkDistance {
    private float dist = 20.0f;
    void Start()
    {
        this.transform.LookAt(new Vector3(0, 0, 0));
        checkCorrectDistance(dist);
    }
    void Update()
    {
        if (Network.isServer)
        {
            dist -= Time.deltaTime;
            networkView.RPC("updateDist4", RPCMode.All, dist);
        }
        checkCorrectDistance(dist);
    }


	void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag.Equals("Player") || col.gameObject.tag.Equals("PlayerBullet"))
        {
            //networkView.RPC("ReduceAttackSpeed", RPCMode.All);
            if (Network.isServer)
            {
                Network.Destroy(gameObject);
            }
        }
    }
    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.name.Contains("Planet"))
        {
            if (Network.isServer)
            {
                Network.Destroy(gameObject);
            }
        }
    }
    [RPC]
    void updateDist4(float val)
    {
        dist = val;
    }

    [RPC]
    void ReduceAttackSpeed()
    {
        gameManager.reloadCount++;
    }
}
