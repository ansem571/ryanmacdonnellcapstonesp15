﻿using UnityEngine;
using System.Collections;

public class fauxGravityBody : MonoBehaviour {

    public fauxGravityAttractor attractor;
    private Transform myTransform;
    private float realDistance = 7.0f;
	void Start () {
        attractor = GameObject.FindGameObjectWithTag("Planet").GetComponent<fauxGravityAttractor>();
        rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        rigidbody.useGravity = false;
        myTransform = transform;
	}
	
	void Update () {
        attractor.Attract(myTransform);
        //checkCorrectDistance(realDistance);
	}

    protected void checkCorrectDistance(float dist)
    {
        realDistance = Vector3.Distance(this.transform.position, Vector3.zero);

        if (realDistance != dist) // arbitrary value will change to get actual distance from elsewhere as a pass in value
        {
            realDistance = dist;
            transform.position = (transform.position - Vector3.zero).normalized * realDistance + Vector3.zero;
        }
    }
}
