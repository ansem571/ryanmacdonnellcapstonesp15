﻿using UnityEngine;
using System.Collections;

public class checkDistance : MonoBehaviour {

    private float realDistance;

    protected void checkCorrectDistance(float dist)
    {
        realDistance = Vector3.Distance(this.transform.position, Vector3.zero);

        if (realDistance != dist) // arbitrary value will change to get actual distance from elsewhere as a pass in value
        {
            realDistance = dist;
            transform.position = (transform.position - Vector3.zero).normalized * realDistance + Vector3.zero;
        }
        transform.LookAt(Vector3.zero);
    }
}
