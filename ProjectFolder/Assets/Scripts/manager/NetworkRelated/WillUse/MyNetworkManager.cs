﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class MyNetworkManager : MyGuiStyles {

    private string registeredGameName = "EarthDestroyerCapstoneAdventure3DTesting";   
    private float refreshRequestLength = 3.0f;
    private HostData[] hostData;
    private List<string> serverPlayers = new List<string>();
    private List<string> serverGuids = new List<string>();
    private List<string> serverColors = new List<string>();
    public List<string> sharedGuids = new List<string>();
    public List<string> sharedColors = new List<string>();
    public List<string> sharedPlayers = new List<string>();
    public int totalPlayers = 0;

    public bool isRefreshing = false;
    public bool invalidPassword = false;
    public bool isSinglePlayer = false;
	
    public void StartServer(string gameName, string description, int connections, string passWord)
    {
        resetPlayerInfo();
        Network.incomingPassword = passWord;
        Network.InitializeServer(connections, 2510, true);
        MasterServer.updateRate = 3;
        if(connections != 1)
        {
            MasterServer.dedicatedServer = true;
            isSinglePlayer = false;
        }
        else
        {
            MasterServer.dedicatedServer = false;
            isSinglePlayer = true;
            description += "\nYou have decided to play the game in single player mode.";
        }
        MasterServer.RegisterHost(registeredGameName, gameName, description);
    }

    public void resetPlayerInfo()
    {
        serverPlayers.Clear();
        serverGuids.Clear();
        serverColors.Clear();

        sharedPlayers.Clear();
        sharedGuids.Clear();
        sharedColors.Clear();
        totalPlayers = 0;
    }

    void addToPlayerList(string n, string guid, string c)
    {
        if (!Network.isServer)
            return;
        bool found = false;
        foreach(string s in serverPlayers)
        {
            if (n.Equals(s))
                found = true;
        }
        if (!found)
        {
            serverPlayers.Add(n);
            serverGuids.Add(guid);
            serverColors.Add(c);
            totalPlayers++;
        }
        updateList();
    }

    void removeFromPlayerList(string guid)
    {
        if (!Network.isServer)
            return;
        int index = -1;
        for(int i = 0; i < serverGuids.Count; i++)
        {
            if (serverGuids[i] == guid)
                index = i;
        }
        if (index != -1)
        {
            serverPlayers.RemoveAt(index);
            serverGuids.RemoveAt(index);
            serverColors.RemoveAt(index);
            totalPlayers--;
        }
        updateList();
    }

    void updateList()
    {
        string pList = "";
        for(int i = 0; i < serverPlayers.Count; i++)
        {
            if(i > 0)
                pList += "|";
            pList += serverPlayers[i];
        }
        networkView.RPC("SVCL_PlayerList", RPCMode.All, pList);

        string gList = "";
        for(int k = 0; k < serverGuids.Count; k++)
        {
            if(k > 0)
                gList += "|";
            gList += serverGuids[k];
        }
        networkView.RPC("SVCL_GuidList", RPCMode.All, gList);

        string cList = "";
        for (int j = 0; j < serverColors.Count; j++)
        {
            if (j > 0)
                cList += "|";
            cList += serverColors[j];
        }
        networkView.RPC("SVCL_ColorList", RPCMode.All, cList);
    }

    [RPC]
    void SV_AddPlayer(string pn)
    {
        if (!Network.isServer)
            return;
        string[] playerInfo = pn.Split("|"[0]);
        addToPlayerList(playerInfo[0], playerInfo[1], playerInfo[2]);
    }

    void OnServerInitialized()
    {
        Debug.Log("Server has been initialized.");
        addToPlayerList(PlayerPrefs.GetString(CreateProfile.us) + ": Host", Network.player.guid, PlayerPrefs.GetString(CreateProfile.colour));
    }

    void OnPlayerDisconnected(NetworkPlayer player)
    {
        //Debug.Log("Player disconnected from: " + player.ipAddress + ":" + player.port + ":" + player.guid);
        removeFromPlayerList(player.guid);
        Network.RemoveRPCs(player);
        Network.DestroyPlayerObjects(player);
    }

    public IEnumerator RefreshHostList()
    {
        //Debug.Log("Refreshing...");
        MasterServer.RequestHostList(registeredGameName);
        //float timeStarted = Time.time;
        float timeEnd = Time.time + refreshRequestLength;

        while (Time.time < timeEnd)
        {
            hostData = MasterServer.PollHostList();
            yield return new WaitForEndOfFrame();
        }

        if(hostData == null || hostData.Length == 0)
        {
            //Debug.Log("No active servers have been found.");
        }
        else
        {
            //Debug.Log(hostData.Length + " has been found.");
        }
        isRefreshing = false;
    }

    void OnMasterServerEvent(MasterServerEvent mse)
    {
        if(mse == MasterServerEvent.RegistrationSucceeded)
        {
            //Debug.Log("Registration was successful");
        }
        if(mse == MasterServerEvent.RegistrationFailedGameName)
        {
            //Debug.Log("Game Name already exists");
        }
    }
    void OnFailedToConnect(NetworkConnectionError error)
    {
        Debug.Log("Could not connect to server: " + error);
        if(error == NetworkConnectionError.InvalidPassword)
        {
            Debug.Log("Invalid password Entered");
        }
    }

    void OnConnectedToServer()
    {
        if (!Network.isServer)
        {
            networkView.RPC("SV_AddPlayer", 
                RPCMode.All, 
                PlayerPrefs.GetString(CreateProfile.us) + "|" + Network.player.guid + "|" + PlayerPrefs.GetString(CreateProfile.colour));
        }
    }

    [RPC]
    void SVCL_PlayerList(string pl)
    {
        string[] temp = pl.Split("|"[0]);
        sharedPlayers = temp.ToList();
    }

    [RPC]
    void SVCL_GuidList(string gl)
    {
        string[] temp = gl.Split("|"[0]);
        sharedGuids = temp.ToList();
    }

    [RPC]
    void SVCL_ColorList(string cl)
    {
        string[] temp = cl.Split("|"[0]);
        sharedColors = temp.ToList();
    }

    void OnDisconnectedFromServer(NetworkDisconnection info)
    {
        if(Network.isServer)
        {
            resetPlayerInfo();
        }
        else
        {
            if(info == NetworkDisconnection.LostConnection)
            {
                Debug.Log("Lost connection");
            }
            else
            {
                Debug.Log("Host left game");

                //need to test to make sure this works like this
                //Application.LoadLevel("main_menu");
            }
        }
    }

    
    void OnApplicationQuit()
    {
        if (Network.isServer)
        {
            Network.Disconnect(200);
            MasterServer.UnregisterHost();
        }
        if (Network.isClient)
        {
            Network.Disconnect(200);
        }
    }

    public HostData[] getHostData()
    {
        return hostData;
    }
}
