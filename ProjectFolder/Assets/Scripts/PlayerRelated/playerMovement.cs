﻿using UnityEngine;
using System.Collections;

public class playerMovement : MonoBehaviour {

    private float moveSpeed = 5f;
    private Vector3 moveDir;
    public GameObject planet;

    void Start()
    {
        planet = GameObject.FindGameObjectWithTag("Planet");
    }

    public void updateMoveDir()
    {
        moveDir = new Vector3(Input.GetAxisRaw("Horizontal"), 0, -Input.GetAxisRaw("Vertical")).normalized;
    }

    public void updateRigid()
    {
        rigidbody.MovePosition(rigidbody.position + transform.TransformDirection(moveDir) * moveSpeed * Time.deltaTime);    
    }
}
