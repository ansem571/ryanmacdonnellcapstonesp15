﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class gameManager : MyGuiStyles{
    private int remaining = 0;
    private const float loadNextWave = 10.0f;
    private float timePast = 0.0f;
    public static int playersFromLobby = 0;
    public static int playersInGame = 0;
    public static int difficulty;
    private bool found = false;
    public static int movementCount = 0;
    public static int reloadCount = 0;
    private const float checkEnemyUnits = 3.0f;
    private float checkTime = 0.0f;
    private float nextPowerUp = 0.0f;
    private bool singlePlayer = false;

	// Use this for initialization
	void Start () 
    {
        if (!Network.isServer)
            Instructions.viewingInstructions = true;
        playersInGame = 0;
        playersFromLobby = LobbyMenu.currentGame.connectedPlayers;
        singlePlayer = LobbyMenu.currentGame.comment.Contains("You have decided to play the game in single player mode.");
        difficulty = -1;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (playersInGame == playersFromLobby)
        {
            if (remaining == 0)
            {
                if (Network.isServer)
                {
                    timePast += Time.deltaTime;
                    networkView.RPC("sendTime", RPCMode.All, timePast);
                    if (timePast >= loadNextWave)
                    {
                        timePast = 0.0f;
                        Upgrade();
                        NextWave();
                    }
                }
            }
            else
            {
                if(Network.isServer)
                {
                    checkTime += Time.deltaTime;
                    if(checkTime >= checkEnemyUnits)
                    {
                        checkTime = 0.0f;
                        networkView.RPC("SendNumOfEnemies", RPCMode.All);
                    }
                    nextPowerUp += Time.deltaTime;
                    if(nextPowerUp >= 5.0f)
                    {
                        nextPowerUp = 0.0f;
                        int n = Random.Range(1, 4) + 1;
                        spawnPowerUp(n);
                    }
                }
            }
        }
	}

    void OnGUI()
    {
        if (!getInit())
            Initialize();
        if (!Instructions.viewingInstructions)
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            for (int i = 0; i < players.Length; i++ )
            {
                if(players[i].GetComponent<lookaway>().playerName.Equals(PlayerPrefs.GetString(CreateProfile.us)))
                    found = true;
            }
            if ((playersInGame != playersFromLobby && !found) && (!Network.isServer || singlePlayer))
            {
                if (!found)
                {
                    if (GUI.Button(new Rect(Screen.width * 0.475f, Screen.height * 0.45f, Screen.width * 0.1f, Screen.height * 0.05f), "Spawn player", GetButtonStyle()))
                    {
                        //Vector3 spawnPos = getSpawnPosition(players.Length);
                        Vector3 spawnPos = new Vector3(0.0f, 0.0f, 5.0f);
                        Network.Instantiate(Resources.Load("modelPrefabs/playerModel"), spawnPos, Quaternion.identity, 0);
                        players = GameObject.FindGameObjectsWithTag("Player");
                        GameObject cam = GameObject.Find("MainCamera");
                        if (cam != null)
                            Destroy(cam);
                    }

                    if (GUI.Button(new Rect(Screen.width * 0.485f, Screen.height * 0.55f, Screen.width * 0.08f, Screen.height * 0.05f), "Instructions", GetButtonStyle()))
                    {
                        Instructions.viewingInstructions = true;
                    }
                }                
            }
            if (playersInGame != playersFromLobby && Network.isServer)
            {
                networkView.RPC("SendNumOfPlayers", RPCMode.All);
            }
            if(playersInGame == playersFromLobby)
            {
                if(remaining == 0)
                {
                    GUI.Label(new Rect(Screen.width * 0.4f, Screen.height * 0.05f, Screen.width * 0.2f, Screen.height * 0.1f), "Time Until Next Round\n" + System.Math.Round((loadNextWave - timePast), 2), GetGameLabelStyle());
                }               
                GUI.Label(new Rect(Screen.width * 0.9f, Screen.height * 0.15f, Screen.width * 0.1f, Screen.height * 0.1f), "Remaining: " + remaining, GetGameLabelStyle());
            }
        }
    }

    private void Upgrade()
    {
        enemy1movement.conditionMet = false;
        DegreeRotation.conditionMet = false;
        planetScript.planetShield = planetScript.planetMaxShield;
        if(planetScript.planetHealth < planetScript.planetMaxHealth)
        {
            planetScript.planetHealth += (planetScript.planetMaxHealth / 4);
            if (planetScript.planetHealth > planetScript.planetMaxHealth)
                planetScript.planetHealth = planetScript.planetMaxHealth;
        }
        planetScript.updateVals = true;
        difficulty++;
    }

    [RPC]
    private void SendNumOfPlayers()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        playersInGame = players.Length;
    }
    [RPC]
    private void SendNumOfEnemies()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        remaining = enemies.Length;
    }
    [RPC]
    private void sendTime(float t)
    {
        timePast = t;
    }
    private Vector3 getSpawnPosition(int n)
    {
        Vector3 pos = new Vector3();
        switch (playersFromLobby)
        {
            case 1:
            {
                switch(n)
                {
                    case 0:
                    {
                        pos = new Vector3(0.0f, 0.0f, 15.0f);
                    }
                    break;
                }
            }
            break;
            case 2://change these variables
            {
                switch(n)
                {
                    case 0:
                    {
                        pos = new Vector3(-1.0f, 0.0f, 15.0f);
                    }
                    break;
                    case 1:
                    {
                        pos = new Vector3(1.0f, 0.0f, 15.0f);
                    }
                    break;
                }
            }
            break;
            case 3:
            {
                switch(n)
                {
                    case 0:
                    {
                        pos = new Vector3(0.0f, 0.0f, 15.0f);
                    }
                    break;
                    case 1:
                    {
                        pos = new Vector3(-1.73f, 0.0f, 16.06f);
                    }
                    break;
                    case 2:
                    {
                        pos = new Vector3(1.73f, 0.0f, -15.33f);
                    }
                    break;
                }
            }
            break;
            case 4:
            {
                switch (n)
                {
                    case 0:
                        {
                            pos = new Vector3(0.0f, 0.0f, 15.0f);
                        }
                        break;
                    case 1:
                        {
                            pos = new Vector3(15.0f, 0.0f, 0.0f);
                        }
                        break;
                    case 2:
                        {
                            pos = new Vector3(15.0f, 0.0f, 0.0f);
                        }
                        break;
                    case 3:
                        {
                            pos = new Vector3(-15.0f, 0.0f, 0.0f);
                        }
                        break;
                }
                break;
            }
        }
        return pos;
    }

    private void NextWave()
    {
        if (Network.isServer)
        {
            for (int i = 0; i < (5 + difficulty); i++)
            {
                Network.Instantiate(Resources.Load("modelPrefabs/enemy1"), new Vector3(0, 0, 10), Quaternion.identity, 0);
            }
            for (int i = 0; i < (5 + difficulty); i++)
            {
                Network.Instantiate(Resources.Load("modelPrefabs/enemy2"), new Vector3(0, 0, 10), Quaternion.identity, 0);
            }

            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
            remaining = enemies.Length;
        }
    }

    private void spawnPowerUp(int n)
    {
        float x = Random.Range(-0.5f, 0.5f);
        float y = Random.Range(-0.5f, 0.5f);
        float z = Random.Range(0.0f, 1.0f);
        switch(n)
        {
            case 1:
            {
                Network.Instantiate(Resources.Load("modelPrefabs/EnemyMovement"), new Vector3(x, y, z), Quaternion.identity, 0);
            }
            break;
            case 2:
            {
                Network.Instantiate(Resources.Load("modelPrefabs/AttackSpeedReduction"), new Vector3(x, y, z), Quaternion.identity, 0);
            }
            break;
            case 3:
            {
                Network.Instantiate(Resources.Load("modelPrefabs/PlanetHealth"), new Vector3(x, y, z), Quaternion.identity, 0);
            }
            break;
            case 4:
            {
                Network.Instantiate(Resources.Load("modelPrefabs/PlayerHealth"), new Vector3(x, y, z), Quaternion.identity, 0);
            }
            break;
        }
    }
}
