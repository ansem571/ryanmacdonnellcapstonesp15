﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CreateProfile : MyGuiStyles{
    public static bool creatingProfile;

    private string profileName = "";
    private string personalColor = "";
    private List<string> errorsList = new List<string>();

    public static string us;
    public static string colour;

	// Use this for initialization
	void Start () {
        creatingProfile = false;
        us = "UserName" + Application.dataPath;
        colour = "Color" + Application.dataPath;
	}
	
	// Update is called once per frame
	void OnGUI () {
        if (!getInit())
            Initialize();
	    if(creatingProfile)
        {
            GUIStyle customBox = CreateCustomBoxColor(new Vector4(1.0f, 1.0f, 1.0f, 0.25f), new Vector3(0, 0, 0), 36);

            GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "Creating Profile", customBox);

            GUI.Label(new Rect(Screen.width * 0.375f, Screen.height * 0.29f, Screen.width * 0.25f, Screen.height * 0.05f), "Profile Name", GetLabelStyle());

            profileName = GUI.TextField(new Rect(Screen.width * 0.45f, Screen.height * 0.35f, 200f, 30f), profileName, 25, GetTextFieldStyle());

            GUI.Label(new Rect(Screen.width * 0.365f, Screen.height * 0.3875f, Screen.width * 0.275f, Screen.height * 0.1f), 
                "Type in the name of color you want\ne.g., black, blue, cyan, gray, green, grey, magenta, red, white, yellow", GetLabelStyle());

            personalColor = GUI.TextField(new Rect(Screen.width * 0.45f, Screen.height * 0.5f, 200f, 30f), personalColor, 7, GetTextFieldStyle());

            for (int i = 0; i < errorsList.Count; i++)
            {
                GUI.Label(new Rect(Screen.width * 0.375f, Screen.height * (0.525f + i * 0.05f), Screen.width * 0.25f, Screen.height * 0.1f), errorsList[i], GetErrorStyle());
            }

            //creates a profile
            if (GUI.Button(new Rect(Screen.width * 0.425f, Screen.height * 0.75f, Screen.width * 0.15f, Screen.height * 0.05f), "Create Profile", GetButtonStyle()))
            {
                errorsList.Clear();
                CheckForErrors();
                if (errorsList.Count == 0)
                {
                    PlayerPrefs.SetString(us, profileName.ToLower());
                    PlayerPrefs.SetString(colour, personalColor.ToLower());
                    PlayerPrefs.Save();
                    creatingProfile = false;
                }
            }

            //returns without having created a profile
            if (GUI.Button(new Rect(Screen.width * 0.425f, Screen.height * 0.85f, Screen.width * 0.15f, Screen.height * 0.05f), "Return to Main Menu", GetButtonStyle()))
            {
                creatingProfile = false;
            }
        }
	}
    private void CheckForErrors()
    {
        if(profileName.Length == 0)
        {
            errorsList.Add("You need to put in a username for your character");
        }
        if(profileName.Equals("Anonymous"))
        {
            errorsList.Add("Cannot use Anonymous as User name");
        }

        if(personalColor.Length == 0)
        {
            errorsList.Add("You need to input a color for your character");
        }

        bool found = false;
        string temp = personalColor.ToLower();

        for(int i = 0; i < getColorList().Count; i++)
        {
            if (temp.Equals(getColorList()[i]))
                found = true;
        }
        if (!found)
            errorsList.Add("The color you entered does not exist in the databases, try simpler colors");
    }

    
}