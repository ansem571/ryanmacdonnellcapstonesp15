﻿using UnityEngine;
using System.Collections;

public class PlayerObject : MonoBehaviour {

    private string myNickName = "";

	void Start () 
    {
        PlayerPrefs.GetString("UserName");
        BroadcastNickName();
	}

    public void BroadcastNickName()
    {
        networkView.RPC("UpdateNickName", RPCMode.All, myNickName);
    }

    public void SendNickNameTo(NetworkPlayer aPlayer)
    {
        networkView.RPC("UpdateNickName", aPlayer, myNickName);
    }

    public void ChangeNickName(string newNickName)
    {
        if (!networkView.isMine)
            return;
        myNickName = newNickName;
        BroadcastNickName();
    }
	
    [RPC]
	void UpdateNickName(string newNickName)
    {
        if (networkView.isMine)
            return;
        myNickName = newNickName;
    }
}
