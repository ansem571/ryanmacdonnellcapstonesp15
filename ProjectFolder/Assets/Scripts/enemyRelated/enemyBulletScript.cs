﻿using UnityEngine;
using System.Collections;

public class enemyBulletScript : MonoBehaviour {
    AudioClip fired;
    AudioClip hit;
    AudioSource source;
	// Use this for initialization
	void Start () 
    {
        fired = (AudioClip)Resources.Load("Music/bullet", typeof(AudioClip));
        source = gameObject.AddComponent<AudioSource>();
        source.clip = fired;
        source.Play();

        hit = (AudioClip)Resources.Load("Music/hit", typeof(AudioClip));
	}
	
	// Update is called once per frame
	void Update () 
    {

	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag.Equals("PlayerBullet"))
        {
            if (Network.isServer)
            {
                Network.Destroy(gameObject);
            }
            else
            {
                Rigidbody rb = GetComponent<Rigidbody>();
                rb.velocity = Vector3.zero;
                rb.angularVelocity = Vector3.zero;
            }
        }
        if (col.gameObject.tag.Equals("Player"))
        {
            if (Network.isServer)
            {
                Network.Destroy(gameObject);
            }
            else
            {
                source = gameObject.AddComponent<AudioSource>();
                source.clip = hit;
                source.Play();
                Rigidbody rb = GetComponent<Rigidbody>();
                rb.velocity = Vector3.zero;
                rb.angularVelocity = Vector3.zero;
            }
        }
    }
    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.name.Contains("Planet"))
        {
            source = gameObject.AddComponent<AudioSource>();
            source.clip = hit;
            source.Play();
            if (Network.isServer)
            {
                if (planetScript.planetShield > 0)
                    planetScript.planetShield--;
                else
                    planetScript.planetHealth--;
                Network.Destroy(gameObject);
            }
            else
            {
                Rigidbody rb = GetComponent<Rigidbody>();
                rb.velocity = Vector3.zero;
                rb.angularVelocity = Vector3.zero;
            }
        }
    }
}
