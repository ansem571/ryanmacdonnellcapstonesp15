﻿using UnityEngine;
using System.Collections;

public class DegreeRotation : checkDistance {

    //movement related values
    private float dist = 20.0f;
    private float degrees;
    private const float updateTime = 1.0f;
    private float pastTime = 0.0f;
    public static bool conditionMet = false;

    //attack related values
    private float reloadSpeed = Random.Range(3.0f, 30.0f);
    private float reloadTime = 0.0f;

    void Start()
    {
        float x = Random.Range(-0.5f, 0.5f);
        float y = Random.Range(-0.5f, 0.5f);
        float z = Random.Range(0.0f, 1.0f);
        this.transform.position = new Vector3(x, y, z);
        degrees = Random.Range(-45.0f, 45.0f);
        float dif = (float)gameManager.difficulty;
        dif *= 0.01f;
        reloadSpeed -= reloadSpeed * dif;
        checkCorrectDistance(dist);
    }

	// Update is called once per frame
	void FixedUpdate () 
    {
        if (Network.isServer)
        {
            if (!conditionMet)
            {
                Movement1();
                Attack();
            }
            if (conditionMet)
            {
                if (dist > 12.5f)
                    Movement2();
                else
                    Movement3();
            }
        }
	}

    private void Movement1()
    {
        if (degrees == 0f)
        {
            degrees = 0.01f;
        }
        pastTime += Time.deltaTime;
        if (pastTime > updateTime - gameManager.difficulty * 0.01 + gameManager.movementCount)
        {
            pastTime = 0.0f;
            transform.RotateAround(Vector3.zero, Vector3.right, degrees * Time.deltaTime);
            transform.RotateAround(Vector3.zero, Vector3.up, 90 * Time.deltaTime);
        }

        checkCorrectDistance(dist);
    }

    private void Movement2()
    {
        if (degrees == 0f)
        {
            degrees = 0.01f;
        }
        pastTime += Time.deltaTime;
        if (pastTime > updateTime - gameManager.difficulty * 0.01 + gameManager.movementCount)
        {
            pastTime = 0.0f;
            transform.RotateAround(Vector3.zero, Vector3.right, degrees * Time.deltaTime);
            transform.RotateAround(Vector3.zero, Vector3.up, 90 * Time.deltaTime);
        }

        dist-= Time.deltaTime;

        checkCorrectDistance(dist);
    }

    private void Movement3()
    {
        dist -= 0.01f;

        checkCorrectDistance(dist);
    }

    private void Attack()
    {
        if (Network.isServer)
            reloadTime += Time.deltaTime;
        if (reloadTime > (reloadSpeed + (gameManager.reloadCount * 0.5f)))
        {
            reloadTime = 0.0f;
            Vector3 rotate = transform.rotation.eulerAngles;
            Vector3 pos = this.transform.position;

            float realDistance = Vector3.Distance(pos, Vector3.zero);

            if (realDistance != dist - 0.5f)
            {
                realDistance = dist - 0.5f;
                pos = (transform.position - Vector3.zero).normalized * realDistance + Vector3.zero;
            }

            GameObject go = Network.Instantiate(Resources.Load("prefabs/enemyBullet"), pos, Quaternion.Euler(rotate), 0) as GameObject;
            go.rigidbody.AddForce(transform.forward * 250);
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag.Equals("PlayerBullet") || col.gameObject.tag.Equals("Player"))
        {
            if (Network.isServer)
            {
                Network.Destroy(gameObject);
            }
        }
    }
    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.name.Contains("Planet"))
        {
            if (Network.isServer)
            {
                planetScript.planetHealth -= 2;
                Network.Destroy(gameObject);
            }
        }
    }
}
