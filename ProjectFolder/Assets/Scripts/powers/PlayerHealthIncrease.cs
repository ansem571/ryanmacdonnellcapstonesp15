﻿using UnityEngine;
using System.Collections;

public class PlayerHealthIncrease : checkDistance {

    private float dist = 20.0f;
    void Start()
    {
        this.transform.LookAt(new Vector3(0, 0, 0));
        checkCorrectDistance(dist);
    }
    void Update()
    {
        if (Network.isServer)
        {
            dist -= Time.deltaTime;
            networkView.RPC("updateDist1", RPCMode.All, dist);
        }
        checkCorrectDistance(dist);
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag.Equals("PlayerBullet"))
        {
            string from = col.gameObject.GetComponent<playerBulletScript>().firedFrom;
            GameObject[] p = GameObject.FindGameObjectsWithTag("Player");
            for (int i = 0; i < p.Length; i++)
            {
                if (p[i].GetComponent<lookaway>().playerName.Equals(from))
                {
                    p[i].GetComponent<lookaway>().health += 1;
                }
            }
            if (Network.isServer)
            {
                Network.RemoveRPCs(this.networkView.viewID);
                Network.Destroy(gameObject);
            }
        }
        if (col.gameObject.tag.Equals("Player"))
        {
            col.gameObject.GetComponent<lookaway>().health += 1;
            if (Network.isServer)
            {
                Network.RemoveRPCs(this.networkView.viewID);
                Network.Destroy(gameObject);
            }
        }
    }
    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.name.Contains("Planet"))
        {
            if (Network.isServer)
            {
                Network.RemoveRPCs(this.networkView.viewID);
                Network.Destroy(gameObject);
            }
        }
    }

    [RPC]
    void updateDist1(float val)
    {
        dist = val;
    }
}
