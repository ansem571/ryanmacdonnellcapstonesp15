﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if(Network.isServer)
        {
            //left
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
            {
                this.transform.position += transform.right * 0.1f * -1;
            }

            //right
            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
            {
                this.transform.position += transform.right * 0.1f;
            }

            //forward
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
            {
                this.transform.position += transform.forward * 0.065f;
            }

            //backward
            if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
            {
                this.transform.position += transform.forward * 0.065f * -1;
            }

            //rise
            if (Input.GetKey(KeyCode.R))
            {
                this.transform.position += transform.up * 0.065f;
            }

            //fall
            if (Input.GetKey(KeyCode.F))
            {
                this.transform.position += transform.up * 0.065f * -1;
            }

            //rotate left
            if (Input.GetKey(KeyCode.Q))
            {
                this.transform.Rotate(Vector3.up * Time.deltaTime * -90.0f);
            }

            //rotate right
            if (Input.GetKey(KeyCode.E))
            {
                this.transform.Rotate(Vector3.up * Time.deltaTime * 90.0f);
            }

            //rotate up
            if (Input.GetKey(KeyCode.Z))
            {
                this.transform.Rotate(Vector3.right * Time.deltaTime * -90.0f);
            }

            //rotate down
            if (Input.GetKey(KeyCode.C))
            {
                this.transform.Rotate(Vector3.right * Time.deltaTime * 90.0f);
            }
        }
	}
}
