﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class highScores : MyGuiStyles {
    public static bool viewingHighScores;
    List<string> playerNames;
    List<int> scores;
	// Use this for initialization
	void Start () {
        viewingHighScores = false;
        playerNames = new List<string>();
        scores = new List<int>();
	}
	
	// Update is called once per frame
	void OnGUI () {
        if (!getInit())
            Initialize();
        if(viewingHighScores)
        {
            GUIStyle customBox = CreateCustomBoxColor(new Vector4(1.0f, 1.0f, 1.0f, 0.25f), new Vector3(0, 0, 0), 36);
            GUIStyle customTextBox = CreateCustomBoxColor(new Vector4(0.5f, 0.5f, 0.5f, 0.75f), new Vector3(0, 0, 0), 36);
            GUIStyle title = GetTitleStyle();
            GUIStyle myLabel = GetLabelStyle();

            GUI.Box(new Rect(Screen.width * 0.35f, Screen.height * 0.2f, Screen.width * 0.325f, Screen.height * 0.6f), "", customTextBox);

            GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "", customBox);
            title.fontSize = 36;

            GUI.Label(new Rect(Screen.width * 0.425f, Screen.height * 0.2f, Screen.width * 0.4f, Screen.height * 0.05f), "High Scores Screen", title);
            string[] scorePair = PlayerPrefs.GetString("Scores", "Nothing Here").Split("|"[0]);
            if (!scorePair[0].Equals("Nothing Here"))
            {
                myLabel.alignment = TextAnchor.MiddleLeft;
                sortScores(scorePair);
                title.fontSize = 28;
                GUI.Label(new Rect(Screen.width * 0.425f, Screen.height * 0.275f, Screen.width * 0.3f, Screen.height * 0.1f),
                        ("Player Name\tScore"), title);
                for(int i = 0; i < playerNames.Count; i++)
                {
                    GUI.Label(new Rect(Screen.width * 0.45f, (Screen.height * (0.3f + i * 0.05f)), Screen.width * 0.4f, Screen.height * 0.1f),
                        (playerNames[i] + "\t" + scores[i]), myLabel);
                }
            }
            else
            {
                GUI.Label(new Rect(Screen.width * 0.375f, Screen.height * 0.3f, Screen.width * 0.275f, Screen.height * 0.05f),
                        "Nothing here yet", myLabel);
            }

            if (GUI.Button(new Rect(Screen.width * 0.45f, Screen.height * 0.7f, Screen.width * 0.125f, Screen.height * 0.05f), "Return to Menu", GetButtonStyle()))
            {
                viewingHighScores = false;
            }
        }
	}

    private void sortScores(string[] scorePair)
    {
        playerNames.Clear();
        scores.Clear();

        foreach (string s in scorePair)
        {
            string[] temp = s.Split(":"[0]);
            playerNames.Add(temp[0]);
            scores.Add(int.Parse(temp[1]));
        }
        for(int i = 0; i < scores.Count; i++)
        {
            for(int j = 0; j < scores.Count - 1; j++)
            {
                if(scores[j] < scores[j+1])
                {
                    int temp = scores[j];
                    scores[j] = scores[j + 1];
                    scores[j + 1] = temp;
                }
            }
        }
    }
}
