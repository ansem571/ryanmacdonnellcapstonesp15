﻿using UnityEngine;
using System.Collections;

public class EnemyMoveDebuff : checkDistance {
    private float dist = 20.0f;
    void Start()
    {
        this.transform.LookAt(new Vector3(0, 0, 0));
        checkCorrectDistance(dist);
    }

    void Update()
    {
        if (Network.isServer)
        {
            dist -= Time.deltaTime;
            networkView.RPC("updateDist3", RPCMode.All, dist);
        }
        checkCorrectDistance(dist);
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag.Equals("Player") || col.gameObject.tag.Equals("PlayerBullet"))
        {
            networkView.RPC("UpdateMovement", RPCMode.All);
            if (Network.isServer)
            {
                Network.RemoveRPCs(this.networkView.viewID);
                Network.Destroy(gameObject);
            }
        }
    }
    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.name.Contains("Planet"))
        {
            if (Network.isServer)
            {
                Network.RemoveRPCs(this.networkView.viewID);
                Network.Destroy(gameObject);
            }
        }
    }
    [RPC]
    void updateDist3(float val)
    {
        dist = val;
    }

    [RPC]
    void UpdateMovement()
    {
        gameManager.movementCount++;
    }
}
