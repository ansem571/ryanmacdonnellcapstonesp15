﻿using UnityEngine;
using System.Collections;

public class playerBulletScript : MonoBehaviour {

    private const float maxLength = 25.0f;
    private float life = 5.0f;
    private float timeAlive = 0.0f;
    public string firedFrom = "";
    AudioClip fired;
    AudioClip hit;
    AudioSource source;
    AudioSource source2;
	// Use this for initialization
	void Start () 
    {
        fired = (AudioClip)Resources.Load("Music/lazer", typeof(AudioClip));
        source = gameObject.AddComponent<AudioSource>();
        source.clip = fired;
        source.Play();
	}
	
	// Update is called once per frame
	void Update () {
        if (Network.isServer)
            timeAlive += Time.deltaTime;
        if (timeAlive >= life && Network.isServer)
            Network.Destroy(this.gameObject);
        float dist = Vector3.Distance(this.transform.position, Vector3.zero);
        if (dist > maxLength && Network.isServer)
            Network.Destroy(this.gameObject);
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag.Equals("Enemy") || col.gameObject.tag.Equals("EnemyBullet"))
        {
            GameObject[] p = GameObject.FindGameObjectsWithTag("Player");
            for (int i = 0; i < p.Length; i++ )
            {
                if(p[i].GetComponent<lookaway>().playerName.Equals(firedFrom))
                {
                    p[i].GetComponent<lookaway>().score += 10;
                    hit = (AudioClip)Resources.Load("Music/hit", typeof(AudioClip));
                    source2 = gameObject.AddComponent<AudioSource>();
                    source2.clip = hit;
                    source2.Play();
                }
            }
            if (Network.isServer)
            {
                Network.Destroy(gameObject);
            }
            else
            {
                Rigidbody rb = GetComponent<Rigidbody>();
                rb.velocity = Vector3.zero;
                rb.angularVelocity = Vector3.zero;
            }
        }
        else if(col.gameObject.tag.Equals("PowerUp"))
        {
            hit = (AudioClip)Resources.Load("Music/hit", typeof(AudioClip));
            source2 = gameObject.AddComponent<AudioSource>();
            source2.clip = hit;
            source2.Play();
            if (Network.isServer)
            {
                Network.Destroy(gameObject);
            }
            else
            {
                Rigidbody rb = GetComponent<Rigidbody>();
                rb.velocity = Vector3.zero;
                rb.angularVelocity = Vector3.zero;
            }
        }
    }
}
