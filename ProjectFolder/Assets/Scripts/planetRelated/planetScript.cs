﻿using UnityEngine;
using System.Collections;

public class planetScript : MyGuiStyles {
    public static int planetHealth;
    public static int planetShield;
    public static int planetMaxHealth;
    public static int planetMaxShield;
    public static bool updateVals = false;
	// Use this for initialization
	void Start () {
        planetMaxHealth = 10;
        planetMaxShield = 10;
        planetHealth = planetMaxHealth;
        planetShield = planetMaxShield;
	}
	
	// Update is called once per frame
	void Update () {
        if(updateVals && Network.isServer)
        {
            networkView.RPC("setPlanetShield", RPCMode.All, planetShield);
            networkView.RPC("setPlanetHealth", RPCMode.All, planetHealth);
        }
	    if(planetShield <= 0 && Network.isServer)
        {
            networkView.RPC("setPlanetShield", RPCMode.All, 0);
            enemy1movement.conditionMet = true;
            DegreeRotation.conditionMet = true;
        }
        if(planetHealth <= 0)
        {
            GameObject[] player = GameObject.FindGameObjectsWithTag("Player");
            int score = -1;
            foreach(GameObject p in player)
            {
                if(p.GetComponent<lookaway>().playerName.Equals(PlayerPrefs.GetString(CreateProfile.us)))
                {
                    score = p.GetComponent<lookaway>().score;
                }
            }
            if (score != -1)
            {
                if (PlayerPrefs.GetString("Scores").Length > 0)
                    PlayerPrefs.SetString("Scores", PlayerPrefs.GetString("Scores") + "|" + PlayerPrefs.GetString(CreateProfile.us) + ":" + score);
                else
                    PlayerPrefs.SetString("Scores", PlayerPrefs.GetString(CreateProfile.us) + ":" + score);
                PlayerPrefs.Save();
            }
            if (Network.isServer)
            {
                Network.Disconnect(200);
                MasterServer.UnregisterHost();
            }
            gameManager.playersInGame = 0;
            gameManager.playersFromLobby = 0;
            Application.LoadLevel("main_menu");
        }
	}
    void OnGUI()
    {
        if(!getInit())
            Initialize();
        GUI.Label(new Rect(Screen.width * 0.9f, 0, Screen.width * 0.1f, Screen.height * 0.1f), "Planet Shield Strength: " + planetShield, GetGameLabelStyle());
        GUI.Label(new Rect(Screen.width * 0.9f, Screen.height * 0.075f, Screen.width * 0.1f, Screen.height * 0.1f), "Planet Health: " + planetHealth, GetGameLabelStyle());
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name.Contains("enemyBullet"))
        {
            if (planetShield > -1)
            {
                if (Network.isServer)
                    networkView.RPC("setPlanetShield", RPCMode.All, planetShield);
            }
            else
            {
                if (Network.isServer)
                {
                    networkView.RPC("setPlanetHealth", RPCMode.All, planetHealth);
                }
            }

        }
        if (col.gameObject.name.Contains("enemy1") || col.gameObject.name.Contains("enemy2"))
        {
            if (Network.isServer)
                networkView.RPC("setPlanetHealth", RPCMode.All, planetHealth);
        }
    }

    public static void updatePlanet()
    {
        planetMaxHealth += 5;
        planetMaxShield += 5;
    }
    [RPC]
    void setMaxHealth(int val)
    {
        planetMaxHealth = val;
    }

    [RPC]
    void setMaxShield(int val)
    {
        planetMaxShield = val;
    }

    [RPC]
    void setPlanetShield(int val)
    {
        planetShield = val;
    }
    [RPC]
    void setPlanetHealth(int val)
    {
        planetHealth = val;
    }
}
