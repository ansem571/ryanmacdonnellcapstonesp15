﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MyGuiStyles : MonoBehaviour {

    private GUIStyle myBoxStyle = null;
    private GUIStyle myLabelStyle = null;
    private GUIStyle myGameLabelStyle = null;
    private GUIStyle myTitleStyle = null;
    private GUIStyle myButtonStyle = null;
    private GUIStyle myScrollStyle = null;
    private GUIStyle myTextFieldStyle = null;
    private GUIStyle myErrorStyle = null;

    private float borderWidth = 5.0f; // is applied to drawline

    private List<string> colorList = new List<string>();
    private bool init = false;

	public void Initialize()
    {
        init = true;
        //makes a style for box
        myBoxStyle = new GUIStyle(GUI.skin.box);
        myBoxStyle.normal.background = MakeTex(2, 2, Color.white);
        myBoxStyle.normal.textColor = Color.black;
        myBoxStyle.fontSize = 36;

        //makes a style for a label
        myLabelStyle = new GUIStyle(GUI.skin.label);
        myLabelStyle.fontSize = 20;
        myLabelStyle.normal.textColor = Color.black;
        myLabelStyle.alignment = TextAnchor.MiddleCenter;

        //makes a style for game labels
        myGameLabelStyle = new GUIStyle(GUI.skin.label);
        myGameLabelStyle.fontSize = 25;
        myGameLabelStyle.normal.textColor = Color.white;
        myGameLabelStyle.alignment = TextAnchor.MiddleCenter;

        //makes a style for a Title label
        myTitleStyle = new GUIStyle(GUI.skin.label);
        myTitleStyle.fontSize = 28;
        myTitleStyle.normal.textColor = Color.black;

        //makes a style for a button
        myButtonStyle = new GUIStyle(GUI.skin.button);
        myButtonStyle.normal.textColor = Color.black;
        myButtonStyle.fontSize = 20;

        //makes a style for scroll window
        myScrollStyle = new GUIStyle(GUI.skin.scrollView);
        myScrollStyle.normal.textColor = Color.black;

        //makes a style for text area
        myTextFieldStyle = new GUIStyle(GUI.skin.textField);
        myTextFieldStyle.normal.textColor = Color.black;
        myTextFieldStyle.fontSize = 20;
        myTextFieldStyle.normal.background = MakeTex(2, 2, Color.cyan);

        //makes a style for the errorList
        myErrorStyle = new GUIStyle(GUI.skin.label);
        myErrorStyle.normal.textColor = Color.red;
        myErrorStyle.fontSize = 16;
        myErrorStyle.alignment = TextAnchor.MiddleCenter;

        //sets up the colorList list
        colorList.Add("black");
        colorList.Add("blue");
        colorList.Add("cyan");
        colorList.Add("gray");
        colorList.Add("green");
        colorList.Add("grey");
        colorList.Add("magenta");
        colorList.Add("red");
        colorList.Add("white");
        colorList.Add("yellow");
    }

    private Texture2D MakeTex(int width, int height, Color col)
    {
        Color[] pix = new Color[width * height];
        for (int i = 0; i < pix.Length; ++i)
        {
            pix[i] = col;
        }
        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();
        return result;
    }
    /// <summary>
    /// Creates a custom box background style, not a GUI.Box
    /// </summary>
    /// <param name="background">floats (0-1): r,g,b,a</param>
    /// <param name="text">floats (0-1): r,g,b</param>
    /// <param name="textSize">int (>=20)</param>
    /// <returns></returns>
    public GUIStyle CreateCustomBoxColor(Vector4 background, Vector3 text, int textSize)
    {
        GUIStyle temp = new GUIStyle(GUI.skin.box);
        temp.normal.background = MakeTex(2, 2, new Color(background.x, background.y, background.z, background.w));
        temp.normal.textColor = new Color(text.x, text.y, text.z);
        temp.fontSize = textSize;
     
        return temp;
    }
    /// <summary>
    /// Creates a custom box background style, not a GUI.Box
    /// </summary>
    /// <param name="color">input a string that is listed in unity's built in Color values</param>
    /// <param name="text">floats (0-1): r,g,b</param>
    /// <param name="textSize">int (>=20)</param>
    /// <returns></returns>
    public GUIStyle CreateCustomBoxColor(string color, Vector3 text, int textSize)
    {
        Color c = getColor(color);
        GUIStyle temp = new GUIStyle(GUI.skin.box);
        temp.normal.background = MakeTex(2, 2, c);
        temp.normal.textColor = new Color(text.x, text.y, text.z);
        temp.fontSize = textSize;

        return temp;
    }

    public Color getColor(string color)
    {
        switch(color)
        {
            case "black":
            {
                return Color.black;
            }
            case "blue":
            {
                return Color.blue;
            }
            case "cyan":
            {
                return Color.cyan;
            }
            case "gray":
            {
                return Color.gray;
            }
            case "green":
            {
                return Color.green;
            }
            case "grey":
            {
                return Color.grey;
            }
            case "magenta":
            {
                return Color.magenta;
            }
            case "red":
            {
                return Color.red;
            }
            case "white":
            {
                return Color.white;
            }
            case "yellow":
            {
                return Color.yellow;
            }
            default:
            {
                return Color.clear;
            }
        }
    }

    public GUIStyle GetBoxStyle()
    {
        return myBoxStyle;
    }
    public GUIStyle GetLabelStyle()
    {
        return myLabelStyle;
    }
    public GUIStyle GetGameLabelStyle()
    {
        return myGameLabelStyle;
    }
    public GUIStyle GetTitleStyle()
    {
        return myTitleStyle;
    }
    public GUIStyle GetButtonStyle()
    {
        return myButtonStyle;
    }
    public GUIStyle GetScrollStyle()
    {
        return myScrollStyle;
    }
    public GUIStyle GetTextFieldStyle()
    {
        return myTextFieldStyle;
    }
    public GUIStyle GetErrorStyle()
    {
        return myErrorStyle;
    }
    public float GetLineWidth()
    {
        return borderWidth;
    }
    public List<string> getColorList()
    {
        return colorList;
    }
    public bool getInit()
    {
        return init;
    }
}
