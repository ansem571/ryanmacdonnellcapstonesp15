﻿using UnityEngine;
using System.Collections;

public class Instructions : MyGuiStyles {

    public static bool viewingInstructions;

	void Start () {
        viewingInstructions = false;
	}

    public void OnGUI()
    {
        if (!getInit())
            Initialize();
        if (viewingInstructions)
        {
            GUIStyle customBox = CreateCustomBoxColor(new Vector4(1f, 1f, 1f, 0.15f), new Vector3(0, 0, 0), 36);
            GUIStyle customTextBox = CreateCustomBoxColor(new Vector4(0.5f, 0.5f, 0.5f, 0.75f), new Vector3(0, 0, 0), 36);
            GUIStyle title = GetTitleStyle();
            GUIStyle myLabel = GetLabelStyle();

            GUI.Box(new Rect(Screen.width * 0.35f, Screen.height * 0.2f, Screen.width * 0.325f, Screen.height * 0.6f), "", customTextBox);

            GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "", customBox);
            title.fontSize = 36;

            GUI.Label(new Rect(Screen.width * 0.425f, Screen.height * 0.2f, Screen.width * 0.4f, Screen.height * 0.05f), "Instructions Screen", title);

            myLabel.alignment = TextAnchor.MiddleLeft;
            GUI.Label(new Rect(Screen.width * 0.375f, Screen.height * 0.3f, Screen.width * 0.4f, Screen.height * 0.4f), 
                "Movement\n" +
                "\tWASD or Arrow Keys to move player\n\n"+
                "Camera\n"+
                "\tPress Tab to change to Third Person View\n"+
                "\tIn Third Person JKLI to rotate camera\n\n" +
                "Destruction\n"+
                "\tCollect or destroy power ups to get added benefits\n"+
                "\tSpace Bar to fire bullet\n"+
                "\tDestroy all enemies to proceed to next wave\n\n"+
                "Survive with your friends as long as you can. Good luck"
                , myLabel);
            if(GUI.Button(new Rect( Screen.width * 0.425f, Screen.height * 0.7f, Screen.width * 0.125f, Screen.height * 0.05f), "Return to Menu", GetButtonStyle()))
            {
                viewingInstructions = false;
            }
        }
    }
}
