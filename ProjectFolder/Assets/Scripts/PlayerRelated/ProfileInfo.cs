﻿using UnityEngine;
using System.Collections;

public class ProfileInfo : MonoBehaviour {
    private string userName;
    private string userColor;
	// Use this for initialization
	void Start () 
    {
        userName = PlayerPrefs.GetString(CreateProfile.us);
        userColor = PlayerPrefs.GetString(CreateProfile.colour);
	}

    void Update()
    {
    }

    public string GetUserName()
    {
        return this.userName;
    }
    public string GetUserColor()
    {
        return this.userColor;
    }
}
