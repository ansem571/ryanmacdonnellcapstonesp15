﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainMenu : MyGuiStyles {
    private List<string> errorsList = new List<string>();

    public void Start()
    {
        //Use to reset UserName values
        //PlayerPrefs.SetString(CreateProfile.us, "Anonymous");
        //PlayerPrefs.Save();
    }
	public void OnGUI()
    {
        if (!getInit())
            Initialize();
        if (!Instructions.viewingInstructions && !CreateProfile.creatingProfile && !highScores.viewingHighScores)
        {
            GUIStyle menuBackGround = CreateCustomBoxColor(new Vector4(1.0f, 1.0f, 1.0f, 0.25f), new Vector3(0.0f, 0.0f, 0.0f), 35); // white background (50% opacity), black text, textSize 20

            GUI.Box(new Rect(Screen.width * 0.365f, Screen.height * 0.275f, Screen.width * 0.25f, Screen.height * 0.65f), "Earth Destroyer Defense", menuBackGround);

            //GUI.Button(new Rect(Screen.width * 0.4f, Screen.height * 0.25f, Screen.width * 0.18f, Screen.height * 0.4f), "");

            if (!PlayerPrefs.GetString(CreateProfile.us, "Anonymous").Equals("Anonymous") && !PlayerPrefs.GetString(CreateProfile.us, "").Equals(""))
            {
                GUI.Label(new Rect(Screen.width * 0.43f, Screen.height * 0.325f, Screen.width * 0.13f, Screen.height * 0.1f), 
                    "Welcome:\n" + PlayerPrefs.GetString(CreateProfile.us), GetLabelStyle());
                GUIStyle colorBox = CreateCustomBoxColor(PlayerPrefs.GetString(CreateProfile.colour), new Vector3(0.0f, 0.0f, 0.0f), 20);
                GUI.Box(new Rect(Screen.width * 0.575f, Screen.height * 0.365f, Screen.width * 0.025f, Screen.height * 0.05f), "", colorBox);
            }
            if (GUI.Button(new Rect(Screen.width * 0.4575f, Screen.height * 0.45f, Screen.width * 0.08f, Screen.height * 0.05f), "Play Game", GetButtonStyle()))
            {
                if (!PlayerPrefs.GetString(CreateProfile.us, "Anonymous").Equals("Anonymous") && !PlayerPrefs.GetString(CreateProfile.us, "").Equals(""))
                {
                    Application.LoadLevel("Lobby_Menu");
                }
                else
                {
                    errorsList.Clear();
                    errorsList.Add("You need to create a profile before you can begin playing");
                }
            }            
            if (GUI.Button(new Rect(Screen.width * 0.449f, Screen.height * 0.55f, Screen.width * 0.1f, Screen.height * 0.05f), "Create Profile", GetButtonStyle()))
            {
                CreateProfile.creatingProfile = true;
                errorsList.Clear();
            }

            //errors found
            for (int i = 0; i < errorsList.Count; i++)
            {
                GUI.Label(new Rect(Screen.width * 0.36f, Screen.height * (0.625f + i * 0.05f), Screen.width * 0.25f, Screen.height * 0.05f), errorsList[i], GetErrorStyle());
            }

            if (GUI.Button(new Rect(Screen.width * 0.4575f, Screen.height * 0.7f, Screen.width * 0.08f, Screen.height * 0.05f), "High Scores", GetButtonStyle()))
            {
                highScores.viewingHighScores = true;
            }
            if (GUI.Button(new Rect(Screen.width * 0.4575f, Screen.height * 0.8f, Screen.width * 0.08f, Screen.height * 0.05f), "Instructions", GetButtonStyle()))
            {
                Instructions.viewingInstructions = true;
            }
        }
    }
}
